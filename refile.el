;;; todo:
;;; * add paging when there are too many subdirectories
;;; 
;;; * prefix arg doesn't work quite right: Meta-- is ok but C-u is
;;;   not.  Maybe has something to do with my bindings.
;;;
;;; * add quick targets: Computer, Lisp, Java etc.
;;; 
;;; * hide cursor

(require 'cl)

(define-derived-mode refile-mode special-mode "Refile"
  "A mode to quickly refile files.")

(defvar refile-initial-location)
(defvar refile-file)
(defvar refile-location)
(defvar refile-last-location)
(defvar refile-file-queue)

(defvar refile-buffer-name "*Refile*")

(defcustom refile-auto-accept-leaf-directory t
  "Auto accept directory which has no subdirectories."
  :type 'boolean)

(define-key refile-mode-map [delete] 'refile-location-back)
(define-key refile-mode-map [backspace] 'refile-location-back)
(define-key refile-mode-map [?q] 'refile-loop)
(define-key refile-mode-map [?Q] 'refile-quit)
(define-key refile-mode-map [return] 'refile-accept)

(define-key refile-mode-map [(control return)]
  (lambda ()
    (interactive)
    (refile-accept 1)))
(define-key refile-mode-map [?!]
  '(lambda ()
     (interactive)
     (refile-set-location refile-last-location)
     (refile-display)))
(define-key refile-mode-map [?$]
  '(lambda ()
     (interactive)
     (refile-set-location refile-initial-location)
     (refile-display)))

(defvar refile-selection-keys (string-to-list "uhetonasidr,c.gpvjmkbxyf"))

(dolist (c refile-selection-keys)
  (define-key refile-mode-map
    (read-kbd-macro (format "%c" c)) 'refile-select))

(defun refile-location-back ()
  (interactive)
  (setq refile-location (file-name-directory (directory-file-name refile-location)))
  (refile-display))

(defun refile-set-location (loc)
  (setq refile-location (file-name-as-directory
			 (expand-file-name loc))))

(defun refile-append-location (loc)
  (refile-set-location (concat refile-location loc)))

(defun refile-append-location-and-redisplay (loc)
  (refile-append-location loc)
  (refile-display))

(defun refile-display-location ()
  (insert refile-location))

(defun refile-accept (arg)
  (interactive "P")
  (let ((new-name (concat refile-location
			  (file-name-nondirectory
			   (if (file-directory-p refile-file)
			       (directory-file-name refile-file)
			     refile-file)))))
    (when arg
      (setq new-name (read-file-name "New name: " new-name)))
    (message "%s" new-name)
    (when nil
      (rename-file refile-fle new-name)))
    (setq refile-last-location refile-location)
    (refile-loop))

(defvar refile-window-configuration)

(defun refile-make-buffer ()
  (setq refile-window-configuration (current-window-configuration))
  (with-current-buffer (get-buffer-create refile-buffer-name)
    ;; pop-to-buffer before display so that display has access to
    ;; correct window size.
    (pop-to-buffer (current-buffer))    
    ;; (refile-display)
    (refile-mode)))

(defun refile-loop ()
  (interactive)
  (cond (refile-file-queue
	 (setq refile-file (pop refile-file-queue))
	 (refile-display))
	(t (refile-quit))))

(defun refile-quit ()
  (interactive)
  (kill-buffer)
  (set-window-configuration refile-window-configuration))

(defvar refile-choices (make-hash-table))

(defun refile-display-choices ()
  (clrhash refile-choices)
  (loop for c in refile-selection-keys
	for target in (refile-compute-targets refile-location)
	for column from 1
	do
	(puthash c target refile-choices)
	(insert (format "[%c] %-25s" c
			(if (refile-auto-accept-target-p target)
			    (propertize target 'face 'bold)
			  target))
		(if (zerop (mod column 3)) "\n" "   "))))

(defun refile-auto-accept-target-p (target)
  (and refile-auto-accept-leaf-directory
       (null (refile-compute-targets
	      (concat refile-location target)))))

(defun refile-select (&optional arg)
  (interactive "P")
  (let ((loc (gethash last-input-event refile-choices)))
    (when loc
      ;; could be optimized (currently i see no reason):
      ;; 
      ;; * don't need to redisplay buffer if we're going to
      ;;   auto-accept
      ;; 
      ;; * could associate auto-accept flag with a choice to avoid
      ;;   recompting
      (if (refile-auto-accept-target-p loc)
	  ;; restore location if user aborts refile-accept
	  (let ((save-location refile-location))
	    (condition-case nil
		(progn (refile-append-location loc)
		       (refile-accept arg))
	      (quit
	       (setq refile-location save-location))))
	  (refile-append-location-and-redisplay loc)))))

(defun refile-compute-targets (directory)
  (let (result
	(directory (file-name-as-directory directory)))
    (dolist (probe (ignore-errors (directory-files directory)))
      (when (and (not (string= "." (substring probe 0 1)))
		 (file-directory-p (concat directory probe)))
	(setq result (cons probe result))))
   result))

(defun refile-display ()
  (with-current-buffer refile-buffer-name
    (let ((inhibit-read-only t))
      (erase-buffer)
      (goto-char (point-min))
      (insert "File: " refile-file "\n")
      (insert "Location: ")
      (refile-display-location)
      (insert "\n\n")
      (refile-display-choices))))

(defun refile (file-name &optional initial-location)
  ;; G -- Possibly nonexistent file name.  This file should exist but
  ;; I can't find any way to read either file name or directory.
  (interactive "GFile to refile: ")
  (refile* (list file-name) initial-location))

(defun refile* (files &optional initial-location)
  (refile-set-location (or initial-location "~"))
  (setq refile-initial-location refile-location)
  (setq refile-file-queue files)
  (refile-make-buffer)
  (refile-loop))

;;; (setq refile-mode-map (make-sparse-keymap))

;; (defun refile-display-choices ()
;;   (let* ((h (- (window-text-height) (line-number-at-pos)))
;; 	 (columns 3)
;; 	 (lines 0)
;; 	 (targets (refile-compute-targets "~"))
;; 	 (column 1)
;; 	 (l targets))
;;     (while l
;;       (if (< lines (- h 1)))
;;       (setq l (cdr l)))
;;     (while (and l (< lines (- h 5)))
;;       (insert (format "[ ] %-25s" (car l))
;; 	      (if (zerop (mod column columns))
;; 		  (progn
;; 		    (setq lines (1+ lines))
;; 		    "\n")
;; 		"  "))
;;       (setq column (1+ column))
;;       (setq l (cdr l)))
;;     (when l
;;       (insert "--- More ---"))))

;; (refile-make-buffer)
;; (refile* (list "~/src/" "~/Downloads"))
